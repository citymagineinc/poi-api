# Pricing API

Api allowing to manipulates pricing grids and products.

## Running with docker

First build the image :
```bash
mvn package && docker build -t citymagine/pricing .
```

Then run the docker image using the following command :

```bash
docker run -e PROFILE=$PROFILE citymagine/pricing
```

$PROFILE environment variable can take the following values :
-   dev
-   uat
-   prod