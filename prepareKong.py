import yaml
import subprocess

def removeAutoTags(dct:dict):
    if isinstance(dct,list):
        for el in dct:
            removeAutoTags(el)
    elif isinstance(dct,dict):
        for key in dct.keys():
            if key == "tags":
                cleanTags = [tag for tag in dct[key] if not(tag.startswith("OAS3"))]
                dct[key] = cleanTags
            else:
                removeAutoTags(dct[key])


def getServiceName(dct:dict):
    if isinstance(dct,list):
        for el in dct:
            v = getServiceName(el)
            if(v):
                return v
    elif isinstance(dct,dict):
        for key in dct.keys():
            if key == "tags":
                rightTag = [tag for tag in dct[key] if tag.startswith("service_name")][0]
                return rightTag.split(":")[1]
            else:
                v = getServiceName(dct[key])
                if(v):
                    return v

def addHost(yaml:dict,service:str):
    yaml["services"][0]["host"] = service+".upstream" 
    yaml["services"][0]["protocol"] = "http"
    yaml["services"][0]["port"] = 80
    yaml["services"][0]["path"] = "/"

def main():
    yamlFile = yaml.load(open(f"./kong/kong.yml","r"),yaml.Loader)
    removeAutoTags(yamlFile)
    serviceName = getServiceName(yamlFile)
    addHost(yamlFile, serviceName)
    yamlFile["_info"] = {}
    yamlFile["_info"]["defaults"] = {}
    yamlFile["_info"]["select_tags"] = [f"service_name:{serviceName}","service",f"{serviceName}"]
    yamlString: str = yaml.dump(yamlFile)
    yamlString.replace("openapi-",serviceName+'-')
    with open(f"./kong/kong.yml","w") as f:
        f.write(yamlString)

if __name__=="__main__":
    main()