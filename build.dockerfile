FROM openjdk:11-jdk-slim
# Build a shell script because the ENTRYPOINT command doesn't like using ENV
RUN apt-get update
RUN apt install python3.9 -y
RUN apt-get install python3-pip -y


# Install insomnia inso
ENV NODE_VERSION=16.13.1
RUN apt-get install -y curl
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
RUN apt-get install libssl-dev libcurl4-openssl-dev
RUN apt-get install build-essential -y
RUN npm i -g insomnia-inso

ENV PATH="/home/node/.npm-global/bin:${PATH}"


#Install deck
RUN curl -sL https://github.com/kong/deck/releases/download/v1.8.2/deck_1.8.2_linux_amd64.tar.gz -o deck.tar.gz
RUN tar -xf deck.tar.gz -C /tmp
RUN cp /tmp/deck /usr/local/bin/

RUN apt-get install maven -y

COPY prepareKong.py prepareKong.py
COPY requirements.txt requirements.txt
RUN pip3 install -r /requirements.txt

COPY . /home/app
ENTRYPOINT ["sh", "-c", "cd /home/app && mvn test -Dspring.profiles.active=test && mvn verify -Dspring.profiles.active=test && python3 ./prepareKong.py"]