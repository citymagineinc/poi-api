FROM openjdk:11-jre-slim
# Build a shell script because the ENTRYPOINT command doesn't like using ENV
ARG JAR_FILE=target/*.jar


RUN adduser --system --group spring
USER spring:spring

COPY ${JAR_FILE} app.jar

EXPOSE 8080
ENV PROFILE=""
ENTRYPOINT ["sh", "-c", "java -jar /app.jar --spring.profiles.active=${PROFILE:-dev}"]