package com.citymagine.poi.web;

import com.citymagine.poi.dto.resquests.CreatePoiRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

@RestController
@RequestMapping("/pois")
@AllArgsConstructor
@Validated
public class PoiController {

    @PostMapping
    public ResponseEntity<Void> createPoi(@RequestBody @Valid CreatePoiRequest request) throws URISyntaxException {
//        SecureUserDetails secureUserDetails = authService.getCurrentUser();
//        Poi poi = PoiService.createPoi(mapper.map(request));
//        URI uri = new URI("/pois/" + poi.getPoiIdentifier());
        URI uri = new URI("/pois/" + UUID.randomUUID().toString());

        return ResponseEntity.created(uri).build();
    }

}
