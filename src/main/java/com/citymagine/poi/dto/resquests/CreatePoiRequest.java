package com.citymagine.poi.dto.resquests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreatePoiRequest {

    @NotNull
    private float longitude;

    @NotNull
    private float latitude;

    private String image;

    @NotBlank
    private String poiType;

    private String providerIdentifier;

    private String domainCode;

    private String comments;
}
