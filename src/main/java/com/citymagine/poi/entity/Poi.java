package com.citymagine.poi.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "B_POI")
@Data
@EqualsAndHashCode(callSuper=false)
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class Poi {

    @NotNull
    private float longitude;

    @NotNull
    private float latitude;

    private byte[] image;

    @NotBlank
    private String poiType;

    private String providerIdentifier;

    @Column(name = "domain_code")
    private String domainCode;

    private String comments;

    @Id
    @Column(name = "poi_id")
    private Integer poiId;

    @Column(name = "IS_ENABLE")
    private Boolean isEnable;

    @Column(name = "SYSTEM_USER")
    private String systemUser;
    @Column(name = "CREATION_USER_ID")
    private Integer creationUserId;
    @Column(name = "CREATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Column(name = "UPDATE_USER_ID")
    private Integer updateUserId;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "IS_DELETED")
    private Boolean isDeleted;

}